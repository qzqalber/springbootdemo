package com.example.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/Mytest")
public class HelloController {
    @Value("${xyx.name}")
    private String myname;
    @RequestMapping("/hello")
    public String index() {
        return myname+"Hello World";
    }
}
